use std::env;
use std::fs::File;
use std::io::Write;
use std::process::{exit, Child, Command};

extern crate xcb;

use chrono;
use env_logger;
use log;
use log::{error, info};
use x11::xlib::{KeySym};
use xcb::{x};

const RCV_LOG: &'static str = "<- ";
const SND_LOG: &'static str = "-> ";

fn main() -> xcb::Result<()> {
    init_logger();
    info!("Starting MWM with PID {} ...", std::process::id());
    let (conn, screen_num) = xcb::Connection::connect(None)?;
    let setup = conn.get_setup();
    let screen = setup.roots().nth(screen_num as usize).unwrap();
    let root_window = screen.root();
    let mut keyboard_mapping: Option<x::GetKeyboardMappingReply> = None;
    let mut modifier_mapping: Option<x::GetModifierMappingReply> = None;

    // Register for events of interest to root window.
    let events_of_interest = [x::Cw::EventMask(
        x::EventMask::BUTTON_PRESS |
        x::EventMask::BUTTON_RELEASE |
        x::EventMask::ENTER_WINDOW |
        x::EventMask::KEY_PRESS |
        x::EventMask::POINTER_MOTION |
        x::EventMask::PROPERTY_CHANGE |
        x::EventMask::STRUCTURE_NOTIFY |
        x::EventMask::SUBSTRUCTURE_NOTIFY |
        x::EventMask::SUBSTRUCTURE_REDIRECT)];
    let cookie = conn.send_request_checked(&x::ChangeWindowAttributes {
        window: root_window,
        value_list: &events_of_interest,
    });
    conn.check_request(cookie)?;

    loop {
        let event = match conn.wait_for_event() {
            Err(xcb::Error::Connection(err)) => {
                panic!("unexpected I/O error: {}", err);
            }
            Err(xcb::Error::Protocol(xcb::ProtocolError::X(x::Error::Font(err), _req_name))) => {
                // may be this particular error is fine?
                error!("{:?}", err);
                continue;
            }
            Err(xcb::Error::Protocol(err)) => {
                panic!("unexpected prot: ocol error: {:#?}", err);
            }
            Ok(event) => event,
        };
        info!("{}{:?}", RCV_LOG, event);
        match event {
            xcb::Event::X(x::Event::EnterNotify(enter_notify_event)) => {
                conn.send_request(&x::SetInputFocus {
                    revert_to: x::InputFocus::None,
                    focus: enter_notify_event.event(),
                    time:  x::CURRENT_TIME,
                });
                conn.flush()?;
            }
            xcb::Event::X(x::Event::KeyPress(key_press_event)) => {
                if let Some(ref keyboard_mapping) = keyboard_mapping {
                    let keycode = key_press_event.detail() as usize;
                    let keysyms_per_keycode = keyboard_mapping.keysyms_per_keycode() as usize;
                    // Calculate the starting index for the keycode
                    // https://www.x.org/releases/X11R7.6/doc/man/man3/XGetKeyboardMapping.3.xhtml
                    // TODO: create member for 8
                    let keysym_index = (keycode-8) * keysyms_per_keycode;
                    let keysym = keyboard_mapping.keysyms()[keysym_index] as u64;
                    info!("key_sym: {}", keysym);
                    if key_press_event.state() == x::KeyButMask::MOD4 && keysym == x11::keysym::XK_s as KeySym {
                        info!("Spawning something ...");
                        let command = "alacritty";
                        let mut args: Vec<&str> = Vec::new();
                        let child_process: Child = Command::new(command)
                            .args(args)
                            .spawn()
                            .expect(&format!("Failed to spawn {}!", command));
                        info!("Spawned process with PID: {}", child_process.id());
                    } else if keysym == x11::keysym::XK_Escape as KeySym {
                        info!("Exiting MWM ...");
                        let this_pid = std::process::id();
                        match kill_child_processes(std::process::id()) {
                            Ok(_) => info!("Successfully killed child processes of PID: {}", this_pid),
                            Err(err) => error!("Error killing child processes of PID {}: {:#?}", this_pid, err),
                        }
                        exit(0);
                    }
                    conn.flush()?;
                }
            }
            xcb::Event::X(x::Event::MapRequest(map_request_event)) => {
                info!("ChangeWindowAttributes");
                let events_of_interest = [x::Cw::EventMask(
                    x::EventMask::ENTER_WINDOW |
                    x::EventMask::KEY_PRESS |
                    x::EventMask::SUBSTRUCTURE_NOTIFY |
                    x::EventMask::SUBSTRUCTURE_REDIRECT)];
                conn.send_request(&x::ChangeWindowAttributes {
                    window: map_request_event.window(),
                    value_list: &events_of_interest,
                });
                info!("ConfigureWindow");
                // https://rust-x-bindings.github.io/rust-xcb/xcb/x/struct.ConfigureWindow.html#structfield.window
                conn.send_request(&x::ConfigureWindow {
                    window: map_request_event.window(),
                    value_list: &[
                        x::ConfigWindow::X(50),
                        x::ConfigWindow::Y(50),
                        x::ConfigWindow::Width(1000),
                        x::ConfigWindow::Height(500),
                    ],
                });
                info!("MapWindow");
                conn.send_request(&x::MapWindow {
                    window: map_request_event.window(),
                });
                conn.flush()?;
            }
            xcb::Event::X(x::Event::ConfigureRequest(map_request_event)) => {
                // https://rust-x-bindings.github.io/rust-xcb/xcb/x/struct.ConfigureWindow.html#structfield.window
                info!("ConfigureWindow");
                conn.send_request(&x::ConfigureWindow {
                    window: map_request_event.window(),
                    value_list: &[
                        x::ConfigWindow::X(50),
                        x::ConfigWindow::Y(50),
                        x::ConfigWindow::Width(1000),
                        x::ConfigWindow::Height(500),
                    ],
                });
                conn.flush()?;
            }
            xcb::Event::X(x::Event::MappingNotify(mapping_notify_event)) => {
                match mapping_notify_event.request() {
                    x::Mapping::Keyboard => {
                        let cookie = conn.send_request(&x::GetKeyboardMapping {
                            count: mapping_notify_event.count(),
                            first_keycode: mapping_notify_event.first_keycode(),
                        });

                        match conn.wait_for_reply(cookie) {
                            Ok(reply) => {
                                keyboard_mapping = Some(reply);
                                info!("Updated keyboard mapping: {:?}", keyboard_mapping);
                            }
                            Err(err) => {
                                error!("Error getting keyboard mapping: {:#?}", err);
                                keyboard_mapping = None;
                                // TODO: Error handling?!
                                continue;
                            }
                        }
                    }
                    x::Mapping::Modifier => {
                        let cookie = conn.send_request(&x::GetModifierMapping {});
                        match conn.wait_for_reply(cookie) {
                            Ok(reply) => {
                                modifier_mapping = Some(reply);
                                info!("Updated modifier mapping: {:?}", modifier_mapping);
                            }
                            Err(err) => {
                                error!("Error getting modifier mapping: {:#?}", err);
                                modifier_mapping = None;
                                // TODO: Error handling?!
                                continue;
                            }
                        }
                    }
                    _ => {}
                }
            }
            _ => {continue;}
        }
    }
}

fn init_logger() {
    match env::var("$HOME") {
        Ok(value) => {
            println!("The value of $HOME is: {}", value);
            File::create(format!("{}/mwm.log", value))
                .expect("Failed to create log file!");
        }
        Err(_) => { eprintln!("$HOME is not set!"); }
    }
    env_logger::builder()
        .format(|buf, record| {
            let timestamp = chrono::Local::now();
            writeln!(
                buf,
                "{} [{}] {}",
                timestamp.format("%Y-%m-%d %H:%M:%S%.3f"),
                record.level(),
                record.args()
            )
        })
        // Apparently this causes the lgger to write to stdio and logfile?
        // Don't know how exactly that works ...
        .write_style(env_logger::WriteStyle::Always)
        .target(env_logger::Target::Stdout)
        .filter_level(log::LevelFilter::Debug)
        .init();
}

fn kill_child_processes(parent_pid: u32) -> Result<(), std::io::Error> {
    Command::new("pkill")
        .arg("-P")
        .arg(parent_pid.to_string())
        .status()
        .map(|_| ())
}
